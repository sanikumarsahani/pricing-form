async function postData() {
    const data = {};
    data.firstname = document.getElementById("name").value;
    data.email = document.getElementById("email").value;
    data.message = document.getElementById("comments").value;

    var requestOptions = {
        method: "POST",
        headers: {
            // CODE: "EUS6UTEU86AA0Z8VBJ23M1MQZ",
            Authorization: "Bearer EUS6UTEU86AA0Z8VBJ23M1MQZ",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(data),
        redirect: "follow",
    };

    try {
        const response = await fetch(
            "https://forms.maakeetoo.com/formapi/75",
            requestOptions
        );
        alert("FORM SAVED SUCCESSFULLY", response);
        $("#myModal").modal("toggle");
    } catch (error) {
        console.log("post error", error);
    }
}




var slider = document.getElementById("range-selector");
var output = document.getElementById("demo");
output.innerHTML = slider.value; // Display the default slider value

var card1 = document.getElementById('card-1');
var card2 = document.getElementById('card-2');
var card3 = document.getElementById('card-3');

card2.style.border = "3px solid black";

// Update the current slider value (each time you drag the slider handle)
slider.oninput = function () {
    output.innerHTML = this.value;
    console.log(this.value)

    if (this.value <= 10) {
        card1.style.border = "3px solid black";
        card2.style.border = "none";
        card3.style.border = "none";
    } else if (this.value >= 11 && this.value <= 20) {
        card2.style.border = "3px solid black";
        card3.style.border = "none";
        card1.style.border = "none";
    } else {
        card3.style.border = "3px solid black";
        card2.style.border = "none";
        card1.style.border = "none";
    }


}

// slider.addEventListener('change', () => {alert('changed')});




// to open chrome with disabled security in mac.
// open -n -a /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --args --user-data-dir="/tmp/chrome_dev_test" --disable-web-security

// to open chrome with dieabled security in windows. open win+r then paste the below and then click.
// chrome.exe --user-data-dir="C://Chrome dev session" --disable-web-security