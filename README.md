#avoid cors error
## To open chrome with disabled security in mac.
<br/> `open -n -a /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --args --user-data-dir="/tmp/chrome_dev_test" --disable-web-security`

## To open chrome with dieabled security in windows. open win+r then paste the below and then click.
<br/> `chrome.exe --user-data-dir="C://Chrome dev session" --disable-web-security`


## video link
The project is deployed here. Open [drive-link](https://drive.google.com/file/d/1XSjJUxGUTtnI5w4n3NmuNB5JA9O8yiF3/view?usp=sharing) 
